*** Settings ***
Documentation  Example test suite for external libraries using Selenium library
...            (https://robotframework.org/Selenium2Library/Selenium2Library.html),
...            different test styles, custom robot keywords and setup/teardown.
Library  SeleniumLibrary
Library  OperatingSystem

Test Setup  Open Browser  ${url}  ${browser}
Test Teardown  Close Browser

Default Tags    ignore

*** Variables ***
${browser}  Chrome
${url}  http://automationpractice.com/index.php
${valid_user}  testtest123@gmail.com
${valid_pwd}  Password@123


*** Test Cases ***
Example
  #Open Browser  ${url}  ${browser}
  Capture Page Screenshot    filename=selenium-screenshot-index.png
  #Close Browser

Valid Login
  Login Should Be Succesful  ${valid_user}  ${valid_pwd}

Invalid Login
  [Template]  Login Should Fail
  invalid             ${valid_pwd}
  ${valid_user}       invalid
  ${valid_user}       ${EMPTY}
  ${EMPTY}            ${valid_pwd}


*** Keywords ***
Login Should Be Succesful  [Arguments]  ${user}  ${password}
  Go To Login Page
  Type Email In Login Field  ${user}
  Type Password In Password Field  ${password}
  Click Element  //*[@id="SubmitLogin"]/span
  Page Should Contain Element  //*[@id="columns"]/div[1]/span[2]

Login Should Fail  [Arguments]  ${user}  ${password}
  Go To Login Page
  Type Email In Login Field  ${user}
  Type Password In Password Field  ${password}
  Click Element  //*[@id="SubmitLogin"]/span
  Page Should Contain Element  //*[@id="authentication"]

Go To Login Page
  Click Element  //*[@id="header"]/div[2]/div/div/nav/div[1]/a
  
Type Email In Login Field  [Arguments]  ${email}
  Wait Until Page Contains Element  //*[@id="email"]
  Input Text  //*[@id="email"]  ${email}

Type Password In Password Field  [Arguments]  ${pwd}
  Wait Until Page Contains Element  //*[@id="passwd"]
  Input Password  //*[@id="passwd"]  ${pwd}