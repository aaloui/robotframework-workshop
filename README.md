# Robot Framework Workshop

## Getting Started

### Requirements
* Python >3 (https://www.python.org/downloads/) with pip
  * python directories in PATH (Windows default=C:\Python37, C:\Python37\Scripts)
  * https_proxy (URL of the proxy, if needed for installation python packages)

### Install Robot Framework
Install robotframework with python package manager pip from console (maybe needs to be started as administrator)
```sh
pip install robotframework==4.0
```
alternatively install from source (https://code.google.com/archive/p/robotframework/downloads)
```sh
python setup.py install
```

Check Installation
```sh
pip --version
robot --version
```

### Choose integrated development environment (IDE)
Generally you are free in choosing your favorite IDE.  
Many developers prefer PyCharm since Robot Framework is python-based. There are also dedicated IDEs for Robot Framework. Some of them are listed below (see more on https://robotframework.org/#tools).  
Lately (2020) the language server protocol for Robot Framework has been developed and is continuously enhanced. Currently the plugin works best with Visual Studio Code.

#### Visual Studio Code (recommended)
* install VS Code (https://code.visualstudio.com/download)
* add plugins
  * [Robot Framework Language Server](https://hub.robocorp.com/knowledge-base/articles/language-server-protocol-for-robot-framework/)
    * [configure plugin](https://github.com/robocorp/robotframework-lsp/blob/robotframework-lsp-0.9.1/robotframework-ls/docs/config.md) ... settings.json should look like
      ```sh
      {
        "robot.language-server.python": [ "C:/Users/bedelmann/AppData/Local/Programs/Python/Python39" ],
        "robot.pythonpath": [
        ".../AppData/Local/Programs/Python/Python39",
        ".../robotframework-workshop/libraries"
        ]  
      }
      ```
#### Others
* [PyCharm](https://www.jetbrains.com/de-de/pycharm/download/#section=windows) with plugin [Robot Framework Language Server](https://forum.robotframework.org/t/pycharm-with-lsp/161)
* [RED (Eclipse based editor with a debugger by Nokia)](https://github.com/nokia/RED)
* [RIDE (Standalone Robot Framework test data editor)](https://github.com/robotframework/RIDE/wiki)
* [Eclipse](https://www.eclipse.org/downloads/download.php?file=/oomph/epp/2019-12/R/eclipse-inst-win64.exe) with plugin [RobotFramework-EclipseIDE](https://github.com/NitorCreations/RobotFramework-EclipseIDE/wiki/Installation)
* * optional: install PyDev (https://www.till.net/technologie/plone-zope-python/python-entwicklung-mit-eclipse)

## Documentation
* General: https://robotframework.org
* User Guide: http://robotframework.org/robotframework/latest/RobotFrameworkUserGuide.html
* Standard Library Documentation: https://robotframework.org/robotframework/
* Other Libraries: https://robotframework.org/#libraries


## Basics and Syntax

### Test Suite Sections
![Robot Framework Syntax](syntax.png)
* __*** Settings***__
  * import libraries, variable files, resource files and defining metadata and setup

* __*** Variables ***__
  * define test suite variables

* __*** Test Cases ***__
  * create test cases from available keywords

* __*** Keywords ***__
  * create user keywords from lower-level keywords
  * Settings for keywords
    * [Documentation]
    * [Tags]
    * [Arguments] (positional, named-only, free named arguments)
    * [Return]
    * [Teardown]
    * [Timeout]

### Cheat Sheet
* separator: **2 spaces** or pipe 
* ${variable} | scalar variable
* @{list} | list variable entry 1 | list variable entry 2
* &{dictionary} | name=me | ${variable}=robo
* %{environment_var}
* #comment
* empty character: ${EMPTY} 
* space character: ${SPACE} 
* Escape sign: \
